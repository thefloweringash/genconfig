#!/usr/bin/env ruby

require 'erb'

class Controller
  attr_reader :name, :config
  def initialize(name, config)
    @name = name; @config = config
  end
end

script_dir = File.dirname(__FILE__)
controllers_dir = File.join(script_dir, "controllers")

unless ARGV.empty?
  erb = ERB.new(File.read(File.join(script_dir, "x360.ini.erb")))
  controllers = ARGV.map do |controller_name|
    controller_config =
      File.read(File.join(controllers_dir, controller_name))
    Controller.new(controller_name, controller_config)
  end
  puts erb.result(binding)
else
  puts "Available controllers"
  Dir[File.join(controllers_dir, "*")].each do |f|
    controller_name = File.basename(f)
    puts " * #{controller_name}"
  end
end
